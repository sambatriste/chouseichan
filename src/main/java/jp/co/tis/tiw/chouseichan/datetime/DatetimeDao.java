package jp.co.tis.tiw.chouseichan.datetime;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;
import org.seasar.doma.boot.ConfigAutowireable;

import java.util.Map;

@ConfigAutowireable
@Dao
public interface DatetimeDao {

    @Select
    Map<String, Object> select();
}
