# インストール後の動作確認

各種プロダクトのインストール後に、アプリケーションの動作確認を行います。


## リポジトリをclone

このリポジトリを`git clone`します。
Gitクライアントは、CLIでも[SourceTree](https://ja.atlassian.com/software/sourcetree)でもなんでもOKです。


## バックエンドの動作確認


### PostgreSQLを起動

Dockerが使用できる方は、以下のコマンドでPostgreSQLを起動してください。

``` sh
cd <project_root>/docker/postgres
docker-compose up
```

Dockerが使えない場合は、ローカルのPostgreSQLを起動してください。

### データベースの起動確認

データベースへの接続確認を行います。

[H2 Database](http://www.h2database.com/) の Webコンソールを使用して、PostgreSQLに接続してみます。

``` sh
cd <project_root>
./mvnw exec:java
```
※Windowsコマンドプロンプトの場合は、`./mvnw`ではなく`mvnw`としてください


ブラウザが起動し、SQLが打てる状態になっていればOKです。

http://192.168.24.58:62297/

のようなURLに遷移します。

うまく表示されない場合、http://localhost:62297/ のように置き換えるとうまくいく場合があります。

ここまでうまくいけば、データベースの起動はOKです。

### サーバアプリケーションを起動

Spring Bootで作成したアプリケーションを起動します。

``` sh
cd <project_root>
./mvnw clean spring-boot:run
```

以下のようなエラーが表示された場合、環境変数`JAVA_HOME`がJDK8等古いJDKを指している可能性があります。
環境変数`JAVA_HOME`をJDK11のインストールディレクトリになるように設定してください。
```
[ERROR] Failed to execute goal org.apache.maven.plugins:maven-compiler-plugin:3.8.0:compile (default-compile) on project chouseichan: Fatal error compiling: --releaseは無効なフラグです -> [Help 1]
```

ブラウザやcurlで[/api/datetime](http://localhost:8080/api/datetime)にアクセスすると、現在日時が返却されます。
``` sh
curl -G http://localhost:8080/api/datetime
```

ここまで動作すれば、サーバーアプリケーションとDBの接続までうまくいっています。

## フロントエンドの確認

``` sh
cd <project_root>/front
npm install  # 初回のみ
npm start
```

しばらくすると、 http://localhost:3000/ でブラウザが起動します。

画面に「こんにちは！」と表示されていればOKです。

画面内のボタンをクリックすると、バックエンドのWebAPIにリクエストが飛び、現在日時を取得できます。
現在日時が画面に表示されれば、フロントエンドからバックエンドへの疎通確認は終了です。

お疲れ様でした。
