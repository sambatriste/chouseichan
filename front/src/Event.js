import React from 'react';
import { Table } from 'antd';
import { Form, Input, Select, Radio, Button, Divider, Typography } from 'antd';

const { Title } = Typography;


const columns = [{
    title: '名前',
    dataIndex: 'name',
    key: 'name',
}, {
    title: '8/7(月) 20:00～',
    dataIndex: '1',
    key: '1',
}, {
    title: '8/8(火) 20:00～',
    dataIndex: '2',
    key: '2',
}, {
    title: '8/9(水) 21:00～',
    dataIndex: '3',
    key: '3',
}];

const dataSource = [{
    key: '1',
    name: '山田',
    '1': '○',
    '2': '×',
    '3': '×'
}, {
    key: '2',
    name: '田中',
    '1': '○',
    '2': '×',
    '3': '△'
}];


class Event extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            candidates: []
        }
    }

    componentDidMount() {
        this.setState({
            candidates: [
                { 'id': 1, date: '8/7(月) 20:00～', vote: 'sankaku' },
                { 'id': 2, date: '8/8(火) 20:00～', vote: 'sankaku' },
                { 'id': 3, date: '8/9(水) 21:00～', vote: 'sankaku' }
            ]
        });
    }

    onChange = (id, value) => {
        const newCandidates = this.state.candidates.map(e => {
            return (e.id === id) ?
                { ...e, vote: value } :
                e;
        });
        this.setState({ candidates: newCandidates });
    };

    onSubmit = (evt) => {
        evt.preventDefault();
        console.log(this.state.candidates);
    }

    render() {
        return (
            <div>
                <Title level={2}>イベント</Title>
                <div>
                    {this.props.match.params.id}
                </div>
                <Table dataSource={dataSource} columns={columns} pagination={false} />

                <Divider />

                <div>
                    <Title level={3}>出欠を入力する</Title>
                    <Form onSubmit={this.onSubmit}>
                        <Form.Item label='名前'>
                            <Input name='participant_name' placeholder='田中' />
                        </Form.Item>

                        <Form.Item label='候補日程'>
                            {this.state.candidates.map(e => (
                                <div key={"candidate-" + e.id}>
                                    <label>{e.date}</label>
                                    <Radio.Group
                                        value={e.vote}
                                        buttonStyle="solid"
                                        onChange={(evt) => this.onChange(e.id, evt.target.value)}>
                                        <Radio.Button value="maru">〇</Radio.Button>
                                        <Radio.Button value="sankaku">△</Radio.Button>
                                        <Radio.Button value="batsu">×</Radio.Button>
                                    </Radio.Group>
                                    <br />
                                </div>
                            ))}
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">登録する</Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        );
    }
}

export default Event;
