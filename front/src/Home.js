import React from 'react';
import { Link } from "react-router-dom";
import { Button, Typography } from 'antd';

const { Title, Paragraph } = Typography;


class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            datetime: ''
        };
        this.onClick = this.onClick.bind(this);
    }


    onClick(evt) {
        fetch('/api/datetime')
            .then(res => res.text())
            .then(txt => this.setState({
                datetime: txt
            }));
    }

    render() {
        return (
            <div>
                <Title level={1}>こんにちは!</Title>
                <Paragraph>
                    このページが表示されているということは"npm start"に成功していますね。
                </Paragraph>
                <Paragraph>
                    次は以下のボタンを押してサーバーとの接続を確認してみましょう。<br/>
                    事前にPostgreSQLとSpring Bootアプリケーションを起動しておきましょう。
                </Paragraph>


                <Title level={4}>PostgreSQL(Docker)</Title>
                <Paragraph code>$ cd {'<'}project-root{'>'}/docker/postgres</Paragraph>
                <Paragraph code>$ docker-compose up</Paragraph>


                <Title level={4}>Spring Boot</Title>
                <Paragraph code>$ cd {'<'}project-root{'>'}</Paragraph>
                <Paragraph code>$ ./mvnw clean spring-boot:run</Paragraph>
                <Button type="primary" htmlType="submit" onClick={this.onClick}>今何時？</Button>
                <br />
                {this.state.datetime}
                <Paragraph>日時が表示されれば接続成功です。</Paragraph>
                <hr />
                <Link to="/event">イベント登録</Link>
            </div>
        );
    }

}

export default Home;