import React from 'react';
import { withRouter } from 'react-router';
import { Form, Input, Button, Typography } from 'antd';

const { Title } = Typography;


class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eventName: '',
            description: '',
            candidates: ''
        };
    }

    jumpToEvent = (id) => {
        this.props.history.push(`/event/${id}`);
    }

    registerEvent(evt) {
        evt.preventDefault();
        console.log(this.state);
        fetch("/event", {
            method: "POST",
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: this.state
        }).then((res) => res.json())
            .then(json => {
                this.props.history.push(`/event/${json.eventId}`);
            });
    }

    render() {
        return (
            <div>
                <Title level={2}>イベント登録</Title>
                <Form onSubmit={evt => this.jumpToEvent(1)}>

                    <Form.Item label="イベント名">
                        <Input value={this.state.eventName}
                            onChange={evt => this.setState({ eventName: evt.target.value })}
                            placeholder='送別会' />
                    </Form.Item>
                    <Form.Item label="説明">
                        <Input.TextArea
                            value={this.state.description}
                            onChange={evt => this.setState({ description: evt.target.value })}
                            placeholder='送別会の日程調整しましょう！出欠〆切は◯日。'>
                        </Input.TextArea>
                    </Form.Item>
                    <Form.Item label="候補日程"
                        extra="※候補日程／日時を入力してください（候補の区切りは改行で判断されます）">
                        <Input.TextArea name='candiates' value={this.state.candidates}
                            placeholder='8/7(月) 20:00～&#13;&#10;8/8(火) 20:00～&#13;&#10;8/9(水) 21:00～'

                            onChange={evt => this.setState({ candidates: evt.target.value })} >
                        </Input.TextArea>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">出欠表を作る</Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }

}

export default withRouter(Home);