package jp.co.tis.tiw.chouseichan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChouseichanApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChouseichanApplication.class, args);
	}

}
