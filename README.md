# 調整ちゃん

## これはなに？

ハンズオン学習用に用意されたスケジュール調整アプリケーションです。
[調整さん](https://chouseisan.com/)のスペックを模倣しています。

## 事前準備

ハンズオンに先立って、事前準備をお願いします。

- [インストールとサインアップ](./docs/env/install-and-signup.md)
- [インストール後の動作確認](./docs/env/post-install-check.md)

