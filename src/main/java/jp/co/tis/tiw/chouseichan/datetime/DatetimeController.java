package jp.co.tis.tiw.chouseichan.datetime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;

@RestController
public class DatetimeController {

    @Autowired
    DatetimeDao datetimeDao;

    @RequestMapping("/api/datetime")
    @Transactional
    public String datetime() {
        Map<String, Object> result = datetimeDao.select();
        return Objects.toString(result.get("date_time"));
    }

}
